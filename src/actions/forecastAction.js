import { doesNotMatch } from "assert";

export const weatherFromApi = (city) =>{
    return async dispatch =>{
        const url = 'http://api.weatherstack.com/current?access_key=d094f47bb20878ba8b1fdfda3c3ebc4c&query='+ city
        console.log(url);
        const response = await fetch(url);
        const data = await response.json();
        if (data){
            dispatch(updateTemperature(data.current.temperature));
            dispatch(updateMessage(null))            
        }else{
            dispatch(updateMessage('city does not seams to match'))
        }
    }
}

export const updateTemperature = (value) =>{
    return{
        type: 'UPDATE_TEMPERATURE',
        value
    }
}

export const updateNewCity = (value) =>{
    return{
        type: 'UPDATE_NEW_CITY',
        value
    }
}
