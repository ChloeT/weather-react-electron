import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "./App.css";
import Forecast from "./Forecast/Forecast";

class App extends React.Component {
  render() {
    //console.log(this.props);
    return (
      <div className="App">
        <Forecast />
      </div>
    );
  }
}

export default App;
