import React from 'react';
import ForecastForm from './ForecastForm';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';

class Forecast extends React.Component{
    render(){
        return (
            <div>
                <ForecastTitle />
                <ForecastResult />
                <ForecastForm />
            </div>
        )
    }
}

export default Forecast