import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { weatherFromApi, updateNewCity } from "../../actions/forecastAction"

class ForecastForm extends React.Component{
    render(){
        return (
            <div>
                <input type='text' defaultValue= {this.props.newCity} onChange={ (event) => this.props.updateNewCity(event.target.value) }></input>
                <button onClick={ (event) => this.props.weatherFromApi(this.props.newCity)}>Search</button>
                <p>{this.props.newCity}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        city: state.forecastReducer.forecast.city,
        newCity: state.forecastReducer.newCity,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        weatherFromApi,
        updateNewCity
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm);
