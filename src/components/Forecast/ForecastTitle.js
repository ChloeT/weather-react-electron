import React from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class ForecastTitle extends React.Component{
    render(){
        return (
            <header>
                <h1>{this.props.city}</h1>
                <p>{this.props.date}</p>
            </header>
        )
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        city: state.forecastReducer.forecast.city,
        date: state.forecastReducer.date
    }
}

export default connect(mapStateToProps)(ForecastTitle);
