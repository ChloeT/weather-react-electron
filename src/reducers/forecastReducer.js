let initialState = {
    forecast: {
        city: "Search your city",
        temperature: null,
        humidity: null,
        minTemp: null,
        maxTemp: null,
        weather_description: null,
        weather_icon: null,
        wind_speed: null,


    },
    date: new Date().toDateString(),
    newCity: "Lyon"
}

export const forecastReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_CITY':
            return {
                ...state,
                city: state.newCity
            }
        case 'UPDATE_NEW_CITY':
            console.log(state.newCity);
            return {
                ...state,
                newCity: action.value
            }
        case 'UPDATE_TEMPERATURE':
            return {
                ...state,
                forecast: {
                    ...state.forecast,
                    temperature: action.value,
                }
            }
        default:    
            return state;
    }
}
