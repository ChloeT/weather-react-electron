import {combineReducers, createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {forecastReducer} from './reducers/forecastReducer'


const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const middlewares = [
    thunk
];

let store = createStore(combineReducers({forecastReducer,}),
    composeEnhancers(applyMiddleware(...middlewares)), 
);
export default store;